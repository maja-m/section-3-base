﻿using System.Collections.Generic;
using Section3Advanced.Models;

namespace Section3Advanced.Services
{
    public class UsersData : IUserData
    {
        private readonly List<User> Users;

        public UsersData()
        {
            User user1 = new User("Jan", "Kowalski", 34, "jankowalski@email.com");
            User user2 = new User("Katarzyna", "Gruszka", 24, "katarzynagruszka@email.com");
            User user3 = new User("Michał", "Porzeczka", 57, "michalporzeczka@email.com");

            Users = new List<User> { user1, user2, user3 };
        }

        public List<User> GetAllUsers()
        {
            return Users;
        }
        
        public List<User> AddUser(string firstName, string lastName, int age, string email)
        {
            User user = new User(firstName, lastName, age, email);
            Users.Add(user);
            return Users;
        }
    }
}
