﻿using System.Collections.Generic;
using Section3Advanced.Models;

namespace Section3Advanced.Services
{
    public interface IUserData
    {
        List<User> GetAllUsers();
        List<User> AddUser(string firstName, string lastName, int age, string email);
    }
}
