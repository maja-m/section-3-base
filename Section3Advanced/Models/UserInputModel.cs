﻿using System.ComponentModel.DataAnnotations;

namespace Section3Advanced.Models
{
    public class UserInputModel
    {
        [DataType(DataType.Text)]
        [Required]
        [MaxLength(75)]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        [Required]
        [MaxLength(75)]
        public string LastName { get; set; }
        
        [Required]
        public int Age { get; set; }

        [EmailAddress]
        [Required]
        [MaxLength(75)]
        public string Email { get; set; }
    }
}
