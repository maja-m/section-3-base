﻿using Microsoft.AspNetCore.Mvc;
using Section3Advanced.Models;
using Section3Advanced.Services;

namespace Section3Advanced.Controllers
{
    public class HomeController : Controller
    {
        public IUserData UsersData;

        public HomeController(IUserData usersData)
        {
            UsersData = usersData;
        }

        public IActionResult Index()
        {
            return View("Index", UsersData.GetAllUsers());
        }
        
        [HttpGet]
        public IActionResult AddUser()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult AddUser(UserInputModel userInputModel)
        {
            if (ModelState.IsValid)
            {
                UsersData.AddUser(userInputModel.FirstName, userInputModel.LastName, userInputModel.Age, userInputModel.Email);
                return View("Index", UsersData.GetAllUsers());
            }

            return View();
        }
    }
}
